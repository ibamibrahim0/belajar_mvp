/*
 * Copyright (C) 2017 MINDORKS NEXTGEN PRIVATE LIMITED
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://mindorks.com/license/apache-v2
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License
 */

package id.wadidaw.proyekin.ui.login;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import javax.inject.Inject;

import id.wadidaw.proyekin.R;
import id.wadidaw.proyekin.ui.base.BaseActivity;
import id.wadidaw.proyekin.utils.CommonUtils;


/**
 * Created by janisharali on 27/01/17.
 */

public class LoginActivity extends BaseActivity implements LoginMvpView, View.OnClickListener {

    @Inject
    LoginMvpPresenter<LoginMvpView> mPresenter;

    Button btnLogin;
    EditText etEmail;
    EditText etPassword;

    public static Intent getStartIntent(Context context) {
        Intent intent = new Intent(context, LoginActivity.class);
        return intent;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        getActivityComponent().inject(this);

        mPresenter.onAttach(LoginActivity.this);

        setUp();
    }


    @Override
    public void loginSuccess() {
        // TODO when the login success
    }

    @Override
    public void onLoginFailed(String string) {
        // TODO when the login failed
    }


    @Override
    protected void setUp() {
        //TODO implement activity setup (UI)
    }

    @Override
    public void onClick(View view) {
        // TODO when user click login button
    }

    private boolean validate() {
        // TODO implement validate function
        // email is error when it's empty and it's not an email
        // password is error when it's less than 5 char
        return false;
    }
}
